#language: pt

Funcionalidade: Validar a criação de novos meios de envio de mensagens
Contexto:
    Dado que eu acesse a tela principal do Whatsapp

Cenario: Nova mensagem
    Quando seleciono o botão nova conversa
    E seleciono um contato da agenda
    Então é apresentada a tela para conversa com o contato selecionado

Cenario: Novo grupo
    Quando seleciono o botão menu
    E o submenu Novo grupo
    E seleciono os contatos desejados
    E seleciono o botão seta
    Entao é apresentada a tela para inserção de uma foto e nome para o novo grupo
    Dado que eu insira a foto e nome para o grupo
    E selecione o botão check
    Entao o novo grupo é apresentado na relação de contatos

Cenario: Nova transmissão
    Quando seleciono o botão menu
    E o submenu Novo grupo
    E seleciono os contatos desejados
    E seleciono o botão seta
    Entao a nova transmissão é apresentado na relação de contatos
