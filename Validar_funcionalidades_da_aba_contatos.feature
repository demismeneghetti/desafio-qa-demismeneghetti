#language: pt

Funcionalidade: Validar a aba contatos
Contexto:
    Dado que eu acesse a tela principal do Whatsapp

Cenario: Pesquisar contato na agenda por nome
    Quando seleciono a aba contatos
    E seleciono a lupa para pesquisa
    E pesquise por um nome
    E selecione o nome pesquisado
    Então é apresentada a tela para conversa com o contato selecionado

Cenario: Pesquisar contato na agenda pela lista
    Quando seleciono a aba contatos
    E deslizo a barra de rolagem
    E seleciono um contato na listagem
    Então é apresentada a tela para conversa com o contato selecionado

Cenario: Criar novo contato
    Quando seleciono o botão novo contado
    Então é apresentada a tela para cadastro de um novo contato
